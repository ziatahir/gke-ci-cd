# kubernetes-cluster-on-gcp

Google Cloud Platform has containerized application management called Google Kubernetes Engine(GKE).

**Build and Deploy -**
https://www.google.com/amp/s/kartaca.com/en/deploy-your-application-directly-from-gitlab-ci-to-google-kubernetes-enginegke/amp/

**For Service account creation -**
https://medium.com/google-cloud/gitlab-continuous-deployment-pipeline-to-gke-with-helm-69d8a15ed910

For creating the service account > add keys to service and download it in json format .

**Encoding service-account json file**
base64 /path/to/credential.json | tr -d \n

create variable - SERVICE_ACCOUNT  with that encoded value

Whole credential.json will be base64 encoded .

**Role to be added to service account**

Kubernetes Engine Developer

Cloud Build Service Account

Storage Admin

Kubernetes Engine Viewer    

